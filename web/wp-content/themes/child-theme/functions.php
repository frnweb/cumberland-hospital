<?php
function child_theme_enqueue_styles() {

 //    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array(), '', 'all'  );
	
	// wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array(), '', 'all'  );//this replaces parent theme style.css file and is required by wordpress

	// wp_enqueue_style( 'slate-css', get_template_directory_uri() . '/assets/css/slate.min.css', array(), '', 'all'  );//eventually we will swap this out with minified version
	
	wp_enqueue_style( 'child-css', get_stylesheet_directory_uri() . '/assets/css/child.min.css', array(), '', 'all'  );//this replaces parent theme style.css file and is required by wordpress
	
	// Enqueue Google Fonts here
    wp_enqueue_style( 'googleFonts', 'https://fonts.googleapis.com/css?family=Work+Sans:400,500|Zilla+Slab:400,600', false );

	//JS
    wp_enqueue_script( 'child-js', get_stylesheet_directory_uri() . '/assets/js/child.min.js', array(), '', true );

    // Enqueue Adobe Typekit here
    // wp_enqueue_script( 'theme_typekit', '//use.typekit.net/jbw1yis.js');


	// WP_localize_script for template directory
    $directory = array( 'stylesheet_directory_uri' => get_stylesheet_directory_uri() );
    wp_localize_script( 'child-js', 'themepath', $directory );
	

    ///If you add new documents to the child theme, you have to make sure none of these names overwrite the parent files, unless that is the intention.

}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_styles' );


//Dequeue Styles
function dequeue_styles() {
    
    // Motion-UI css
    wp_dequeue_style( 'motion-ui-css' );
    wp_deregister_style( 'motion-ui-css' );

    // Foundation css
    wp_dequeue_style( 'foundation-css' );
    wp_deregister_style( 'foundation-css' );

    // Slate css
    wp_dequeue_style( 'slate-css' );
    wp_deregister_style( 'slate-css' );

    // FRN Plugin css
    wp_dequeue_style( 'frn_plugin_styles' );
    wp_deregister_style( 'frn_plugin_styles' );
}
add_action( 'wp_print_styles', 'dequeue_styles' );

//Dequeue Scripts
function dequeue_scripts() {
    
    // jQuery
    wp_dequeue_script( 'jQuery' );
    wp_deregister_script( 'jQuery' );

    // what-input js
    wp_dequeue_script( 'what-input' );
    wp_deregister_script( 'what-input' );

    // Slate js
    wp_dequeue_script( 'slate-js' );
    wp_deregister_script( 'slate-js' );

    // Foundation js
    wp_dequeue_script( 'foundation-js' );
    wp_deregister_script( 'foundation-js' );

}
add_action( 'wp_print_scripts', 'dequeue_scripts' );




// Checks if typekit script exists (theme_typekit), adds additional script to header.
function theme_typekit_inline() {
  if ( wp_script_is( 'theme_typekit', 'done' ) ) { ?>
  	<script>try{Typekit.load({ async: false });}catch(e){}</script>
<?php }
}
add_action( 'wp_head', 'theme_typekit_inline' );



// Remove auto p from widget area
remove_filter('widget_text_content', 'wpautop');


// Adds class to the body using the page slug/title
add_filter( 'body_class', 'sk_body_class_for_pages' );

function sk_body_class_for_pages( $classes ) {
    if ( is_singular( 'page' ) ) {
        global $post;
        $classes[] = 'page-' . $post->post_name;
    }
    return $classes;
}


// function for excluding a category from a Posts Page
// v v
// function exclude_category( $query ) {
// 	///this hides the news category on the Resources page
//     if ( $query->is_home() && $query->is_main_query() ) {
//         $query->set( 'cat', '-6' );
//     }
// }
// add_action( 'pre_get_posts', 'exclude_category' );

//this adds staff sidebar
function sidebar_widgets_init() {

register_sidebar( array(
        'id' => 'sidebar-staff',
        'name' => __('Staff Sidebar', 'jointswp'),
        'description' => __('The staff single page sidebar.', 'jointswp'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4 class="widgettitle">',
        'after_title' => '</h4>',
) );

}

add_action( 'widgets_init', 'sidebar_widgets_init' );

//end staff sidebar


// Change Post Excerpt Length

function custom_excerpt_length( $length ) {
 	return 37;
 }
 add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



//function for adding breadcrumbs to site
//function get_breadcrumb() {
//    
//    
//    echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
//    if (is_page()) {
//        echo " / ";
//        echo the_title();
//    }
//}

function get_breadcrumb() {
	/* === OPTIONS === */
	$text['home']     = 'Home'; // text for the 'Home' link
	$text['page']     = 'Page %s'; // text 'Page N'
	$text['cpage']    = 'Comment Page %s'; // text 'Comment Page N'
	$sep            = '/'; // separator between crumbs
	$sep_before     = '<span class="sep">'; // tag before separator
	$sep_after      = '</span>'; // tag after separator
	$show_home_link = 1; // 1 - show the 'Home' link, 0 - don't show
	$show_on_home   = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
	$show_current   = 1; // 1 - show current page title, 0 - don't show
	$before         = '<span class="current">'; // tag before the current crumb
	$after          = '</span>'; // tag after the current crumb
	/* === END OF OPTIONS === */
	global $post;
	$home_url       = home_url('/');
	$link_before    = '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
	$link_after     = '</span>';
	$link_attr      = ' itemprop="item"';
	$link_in_before = '<span itemprop="name">';
	$link_in_after  = '</span>';
	$link           = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
	$frontpage_id   = get_option('page_on_front');
	$parent_id      = ($post) ? $post->post_parent : '';
	$sep            = ' ' . $sep_before . $sep . $sep_after . ' ';
	$home_link      = $link_before . '<a href="' . $home_url . '"' . $link_attr . ' class="home">' . $link_in_before . $text['home'] . $link_in_after . '</a>' . $link_after;
	if (is_home() || is_front_page()) {
		if ($show_on_home) echo $home_link;
	} else {
		if ($show_home_link) echo $home_link;
		if ( is_category() ) {
			$cat = get_category(get_query_var('cat'), false);
			if ($cat->parent != 0) {
				$cats = get_category_parents($cat->parent, TRUE, $sep);
				$cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
				if ($show_home_link) echo $sep;
				echo $cats;
			}
			if ( get_query_var('paged') ) {
				$cat = $cat->cat_ID;
				echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
			}
		} elseif ( is_search() ) {
			if (have_posts()) {
				if ($show_home_link && $show_current) echo $sep;
				if ($show_current) echo $before . sprintf($text['search'], get_search_query()) . $after;
			} else {
				if ($show_home_link) echo $sep;
				echo $before . sprintf($text['search'], get_search_query()) . $after;
			}
		} elseif ( is_day() ) {
			if ($show_home_link) echo $sep;
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
			echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
			if ($show_current) echo $sep . $before . get_the_time('d') . $after;
		} elseif ( is_month() ) {
			if ($show_home_link) echo $sep;
			echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
			if ($show_current) echo $sep . $before . get_the_time('F') . $after;
		} elseif ( is_year() ) {
			if ($show_home_link && $show_current) echo $sep;
			if ($show_current) echo $before . get_the_time('Y') . $after;
		} elseif ( is_single() && !is_attachment() ) {
			if ($show_home_link) echo $sep;
			if ( get_post_type() != 'post' ) {
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				printf($link, $home_url . $slug['slug'] . '/', $post_type->labels->singular_name);
				if ($show_current) echo $sep . $before . get_the_title() . $after;
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, $sep);
				if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
				echo $cats;
				if ( get_query_var('cpage') ) {
					echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
				} else {
					if ($show_current) echo $before . get_the_title() . $after;
				}
			}
		// custom post type
		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			if ( get_query_var('paged') ) {
				echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_current) echo $sep . $before . $post_type->label . $after;
			}
		} elseif ( is_attachment() ) {
			if ($show_home_link) echo $sep;
			$parent = get_post($parent_id);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			if ($cat) {
				$cats = get_category_parents($cat, TRUE, $sep);
				$cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
				echo $cats;
			}
			printf($link, get_permalink($parent), $parent->post_title);
			if ($show_current) echo $sep . $before . get_the_title() . $after;
		} elseif ( is_page() && !$parent_id ) {
			if ($show_current) echo $sep . $before . get_the_title() . $after;
		} elseif ( is_page() && $parent_id ) {
			if ($show_home_link) echo $sep;
			if ($parent_id != $frontpage_id) {
				$breadcrumbs = array();
				while ($parent_id) {
					$page = get_page($parent_id);
					if ($parent_id != $frontpage_id) {
						$breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
					}
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse($breadcrumbs);
				for ($i = 0; $i < count($breadcrumbs); $i++) {
					echo $breadcrumbs[$i];
					if ($i != count($breadcrumbs)-1) echo $sep;
				}
			}
			if ($show_current) echo $sep . $before . get_the_title() . $after;
		} elseif ( is_tag() ) {
			if ( get_query_var('paged') ) {
				$tag_id = get_queried_object_id();
				$tag = get_tag($tag_id);
				echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_current) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
			}
		} elseif ( is_author() ) {
			global $author;
			$author = get_userdata($author);
			if ( get_query_var('paged') ) {
				if ($show_home_link) echo $sep;
				echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
			} else {
				if ($show_home_link && $show_current) echo $sep;
				if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
			}
		} elseif ( is_404() ) {
			if ($show_home_link && $show_current) echo $sep;
			if ($show_current) echo $before . $text['404'] . $after;
		} elseif ( has_post_format() && !is_singular() ) {
			if ($show_home_link) echo $sep;
			echo get_post_format_string( get_post_format() );
		}
	}
} // end of dimox_breadcrumbs()

