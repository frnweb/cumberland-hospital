// Put your custom Javacsrip functions here. Make sure they don't conflict with the Custom.js file in the Parent theme.

//Animates sections when they are reached on scroll- based on viewportchecker.js

$(document).ready(function() {
	"use strict";
	jQuery('.animated-section').addClass("hidden").viewportChecker({
    classToAdd: 'visible animated fadeInUp', // Class to add to the elements when they are visible
    offset: 100
	});
});


// $(document).ready(function() {
  // // Fix sticky nav on small and medium
  // var sticky = $("header.header#globalheader");
  // var breakpoint = Foundation.MediaQuery.current;

  // if (Foundation.MediaQuery.atLeast('medium')) {

  // } else {
  //   sticky.unwrap();
  //   sticky.foundation('destroy');
  // }
// });

// Mobile Phone Call Popover
$(document).ready(function() {
    //this animates the phone number in at the top on mobile
  "use strict";
    var mphone = $("#mobilephone");
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 105) {
            mphone.removeClass('relative').addClass("fixed animated slideInDown");
        } else {
            mphone.removeClass("fixed animated slideInDown").addClass('relative');
        }
    });
});


// $(document).ready(function() {

//   // adding data-attr to nav items
//   var $navitems = $('.header #top-bar-menu #menu-global-nav-2 li a');
//   $("#menu-global-nav-2 li a").attr("data-hover", function(i, origValue){
//     return $(this).html(); 
//   });


//   // Dropdown for Main Navigation
//   $('#menu-global-nav-2').attr("data-closing-time", "0");

//   $('#menu-global-nav-2').on('show.zf.dropdownmenu', function (ev, $el) {
//     $el.css('display', 'block');
//     MotionUI.animateIn($el, 'fade-in fast');
//   });

//   $('#menu-global-nav-2').on('hide.zf.dropdownmenu', function () {
//     $('li ul.menu.vertical.submenu.is-dropdown-submenu').css('display', 'none');
//     MotionUI.animateOut($('li ul'), 'fade-out fast');
//   });
//   // /Dropdown

// });



////equalizes some of the modules that have to be initialized after the load 
// Duo Gallery
$(document).ready(function(){
  "use strict";
	
///DUO equalizers 
  $('.duo--image .row').attr("data-equalizer", "duo__equalizer");
  $('.duo--image .row').attr("data-equalize-on", "medium");
  
  $('.duo--video .row').attr("data-equalizer", "duo__equalizer");
  $('.duo--video .row').attr("data-equalize-on", "medium");
  $('.duo--video .embed-responsive-item').attr("data-equalizer-watch", "duo__equalizer");
  $('.duo--video .flex-video').attr("data-equalizer-watch", "duo__equalizer");
 
  $('.duo--gallery .row').attr("data-equalizer", "duo__equalizer");
  $('.duo--gallery .row').attr("data-equalize-on", "medium");
  $('.duo--gallery .bx-viewport').attr("data-equalizer-watch", "duo__equalizer");
  $('.duo--gallery li').attr("data-equalizer-watch", "duo__equalizer");
  $('.duo--gallery li').foundation();

  $('.duo .row').foundation();

///Carousel equalizers 
  $('.carousel--custom .bx-viewport').attr("data-equalizer", "carousel--custom");
  $('.carousel--custom .bx-viewport').attr("data-equalize-on", "medium");
  $('.carousel--custom .bx-viewport').foundation();

  // Staff Carousels
  $('.staff--carousel .bx-viewport').attr("data-equalizer", "carousel--staff");
  $('.staff--carousel .bx-viewport').attr("data-equalize-on", "medium");
  $('.staff--carousel .bx-viewport').foundation();

	//carousel controls
	$('.carousel--pageitems .bx-viewport').attr("data-equalizer", "column");
    $('.carousel--pageitems .bx-viewport').attr("data-equalize-on", "medium");
    $('.carousel--pageitems .bx-viewport').foundation();
    $('.button--pageitems').css({
        "position": "absolute",
        "bottom": "0px",
        "left": "50%",
        "transform": "translateX(-50%)"
    });
});

$(document).ready(function() {
    var reInitCount = 0;
    window.setTimeout(function() {
        if (reInitCount < 6) {
            Foundation.reInit('equalizer');
        }
        
        reInitCount++;
    }, 2000);
});




