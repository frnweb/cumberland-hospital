<?php 

//Start defining variables for building the header
$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');

// Config the data-toggle for the hamburger button
if($mobilenav == 'offcanvas') {
	$navtoggle = 'off-canvas';
}
if($mobilenav == 'popover') {
	$navtoggle = 'popover';
}


echo '<div class="title-bar" data-responsive-toggle="top-bar-menu" data-hide-for="large">';
	echo get_template_part( 'parts/pieces/header', 'logo' );
        
        echo //PHONE
        get_template_part('parts/pieces/header', 'phone');
        ?>


<!--start the mobile menu button -->
<?php
echo '<a data-toggle="'.$navtoggle.'">';?>
<div id="mobilemenu" class=" small-3 medium-3 columns">
	<ul class="menu">
		<li>
			<?php
			// Hamburger button with Data Toggle
			echo '<button type="button" class="hamburger nav-button hamburger--squeeze" onClick="ga("send", "event", "hamburger Menu", "opens mobile menu button");">';
			?>
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button><!-- /.hamburger.nav-button.hamburger -->
		</li>
		
		<li>
			<?php
				echo _e( 'Menu', 'jointswp' );
			?>
		</li>
		
	</ul><!-- end ul.menu -->
</div><!-- end #mobilemenu -->

<?php
echo '</a>';

	echo '</div>';
?>

<div id="mobilephone" class="show-for-small-only">We're Here to Help <strong><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sticky Header bar on Mobile"]'); ?></strong></div><!-- end mobile phone -->