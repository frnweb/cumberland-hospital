<?php 
			$staffphoto = get_field('staff_photo');
			$firstname = get_field('first_name');
			$lastname = get_field('last_name');
			$title = get_field('title');
			$certifications = get_field('certifications');
					
				echo '<article id="post-'.get_the_ID().'"';
				echo post_class('');
				echo 'role="article" itemscope itemtype="http://schema.org/BlogPosting">';
				echo '<section class="entry-content staff__member" itemprop="articleBody">';
				echo '<div class="row expanded collapse">';
					if( !empty($staffphoto) ){
					 echo '<div class="columns large-8 medium-9 small-12 staff-content">';
					 echo '<div class="staff__member__info">';
                                         echo '<h2>';
						   if ($firstname) {
							echo $firstname;
							}
							if ($lastname) {
							echo '&nbsp;'.$lastname.'';
							}
							if ($certifications) {
							echo '<span class="certifications">&nbsp;'.$certifications.'</span>';
							}
					 echo '</h2>';
						 if ($title) { 
							 	echo '<h3>'.$title.'</h3>';
						 }
					 echo '<div class="staff__member__info__photo">';	
					 echo '<img src="'.$staffphoto['url'].'" alt="'.$staffphoto['alt'].'" />';
					 echo '</div>';//end staff__member__info__photo	
				 
					 echo '</div>';//end staff__member__info
					 echo the_content();
					 echo '</div>'; //end the first column with the bio

					}
					
					else {//conditional statement for if they don't have a bio photo 
						echo '<div class="columns large-8 medium-9 small-12 staff-content">';
						echo '<h2>';
						   if ($firstname) {
							echo $firstname;
							}
							if ($lastname) {
							echo '&nbsp;'.$lastname.'';
							}
							if ($certifications) {
							echo '<span class="certifications">&nbsp;'.$certifications.'</span>';
							}
					 echo '</h2>';
						 if ($title) { 
							 	echo '<h3>'.$title.'</h3>';
						 }
					 echo the_content();
					 echo '</div>'; //end the first column with the bio
					}///end the else conditional statement
                                        
                                        //begin sidebar code
					echo '<div id="sidebar1" class="sidebar large-4 medium-3 columns">';
					echo '<div id="sidebar-container" data-sticky-container class="show-for-medium">';
					echo '<div id="sidebarcontent" class="sticky" data-sticky data-anchor="inner-content">';
					echo '<div class="sidebarinside clearfix">';
					dynamic_sidebar('sidebar-staff');
					echo '</div>'; //end sidebarinside
					echo '</div>'; //end sidebar-container
					echo '</div>'; //end sidebar-content
					echo '</div>'; //end column

					//end sidebar code
					
					echo '</div>'; //end row 
					echo '</section>';//end section
					echo '</article>';//end article
					

?>