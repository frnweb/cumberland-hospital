<!-- Search Icon or Search Bar -->
<?php

$navtype = get_field('nav_type', 'option');
$mobilenav = get_field('mobile_navtype', 'option');
$searchtype = get_field('search_type', 'option');
$dropdowntype = get_field('dropdown_type', 'option');

// Configure opening #usernav classes
if($navtype == 'default') {
	$searchclasses = ' class="search search--default"';
}
if($navtype == 'title-bar') {
	$searchclasses = ' class="search search--title-bar"';
}
if($navtype == 'stacked') {
	$searchclasses = ' class="search search--stacked"';
}

// print header
echo '<div id="search-nav"'.$searchclasses.'>'; ?>	

	<div class="button--search--header show-for-medium">

        <?php get_search_form()?>

</div><!-- end the search button in the top header -->

</div><!-- /#search-nav -->