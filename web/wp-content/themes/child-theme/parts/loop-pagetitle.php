<?php 
/* This is where we are defining how the page titles are appearing throughout different parts of the site*/
?>

<?php $backgroundImg = wp_get_attachment_image_src(
        get_post_thumbnail_id($post->ID), 'full')?>

<header class="article-header" <?php if (is_page_template('template-full-width.php') && has_post_thumbnail() ) { echo 'style="background-image: url( '; echo $backgroundImg[0]; echo')"' ;} elseif ( is_home () || is_archive()){ echo 'style="background-image: url( '; echo get_stylesheet_directory_uri(); echo '/assets/images/compressed/Cumberland-Articles.min.jpg)"';}?>>

  
  <?php 
				if ( is_front_page() && is_home() ) {
				  // Default homepage
					} 
				elseif ( is_front_page() ) {
				  // static homepage
					} 
				elseif ( is_home()) {
				  // blog page and archive page
					echo '<div class="inner"><div class="page-title-container"><div class="page-title-inner"><h1 class="page-title">Articles</h1><p>This page contains original posts intended to keep you informed of the latest relevant news in our field and at Cumberland Hospital.</p></div></div></div>';
					} 
		
				elseif ( is_search() ) {
					//show the search string 
                                        echo '<p><a href="/">Home</a> / Search Results</p>';
					echo '<h1 class="page-title">';
					_e( 'Search Results for: ', 'jointswp' ); 
					echo esc_attr(get_search_query());
					echo '</h1>';
					}
				
				elseif ( is_archive() ) {
                                        echo '<div class="inner"><div class="page-title-container"><div class="page-title-inner"><h1 class="page-title">';
					echo the_archive_title();
					echo '</h1><p>This page contains original posts intended to keep you informed of the latest relevant news in our field and at Cumberland Hospital.</p></div></div></div>';
					//echo the_archive_description('<div class="taxonomy-description">', '</div>');
					}
                                        
                                 elseif (is_singular( 'staff' )) {
                                    //staff-member template
                                  echo '<div class="breadcrumb"><p><a href="/">Home</a> / <a href="/about">About</a> / <a href="/about/staff">Staff</a> / ';
                                  echo get_the_title();
                                  echo '</p></div>';
                                }
                                
                                elseif (basename(get_page_template()) === 'page.php') {
                                    //default page template
                                  echo '<div class="breadcrumb"><p>';
                                  echo get_breadcrumb();
                                  echo '</p></div>';
                                  echo '<h1 class="page-title">'.get_the_title().'</h1>'; 
                                }
                                
                                elseif (is_page_template( 'template-staff-members.php' )) {
                                    //staff-member template
                                  echo '<div class="breadcrumb"><p>';
                                  echo get_breadcrumb();
                                  echo '</p></div>';
                                  echo '<h1 class="page-title">'.get_the_title().'</h1>'; 
                                }
                                
                              
                                
				else {
                                  echo '<div class="inner"><div class="page-title-container"><div class="page-title-inner"><h1 class="page-title">'.get_the_title().'</h1>'; 
                                  if (get_field('blurb_text')){
                                      echo '<p>'.get_field('blurb_text').'</p>';   
                                  }
                                  echo '</div></div></div>';
                                }
	?>
  
</header><!-- end .article header -->