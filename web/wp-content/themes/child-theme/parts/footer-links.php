<?php

$addhelpfullinks = get_field('add_helpful_links');
$choosehelpfullinks = get_field('choose_helfpul_links');

if ( $addhelpfullinks){?>
<div class="module duo helpful-links">
    <div class="inner expanded">
        <div class="row expanded collapse" data-equalizer="duo__equalizer" data-equalize-on="medium" data-resize="duo__equalizer">
            <div class="duo__media duo__media--left large-6 medium-12 columns" data-equalizer-watch="duo__equalizer">
                <div class="duo__img" data-equalizer-watch="duo__equalizer" style="background-image:url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/Cumberland-cafeteria.min.jpg)"></div>         
            </div>
            <div class="duo__content duo__content--right large-6 medium-12 columns" data-equalizer-watch="duo__equalizer">
                <div class="card card--duo">
                    <h1 class="card__header card__header--duo">Helpful Links</h1>
                        <ul>
                        <?php if( have_rows('choose_helpful_links') ) {
                             while( have_rows('choose_helpful_links') ) { the_row();

                             $link = get_sub_field('link');
                             $target = get_sub_field('target'); ?>

                        <li><a href="<?php echo $target ; ?> "> <?php echo $link ; ?></a></li>
                             <?php }//end while have rows
                        }//end if have rows ?>
                        </ul>
                </div><!-- end card--duo -->    
            </div><!--end duo__content -->  
        </div><!-- end row -->
    </div><!-- end inner -->
</div><!-- end module duo helpful links -->
<?php }//end if helpful links
?>

