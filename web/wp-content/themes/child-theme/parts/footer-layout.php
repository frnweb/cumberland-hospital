<?php
// This is Footer Layout

$navtype = get_field('nav_type', 'option');
$footerlogo = get_field('footer_logo', 'option');
$sitetitle = get_bloginfo('name');

// Footer Location
get_template_part('parts/footer', 'location');

get_template_part('parts/footer', 'links');?>

<footer class="footer" role="contentinfo">          
    <div class="inner section">
        
        <div class="row footer-branding expanded large-collapse medium-collapse">
        	
            <!-- Piece-Socials -->
            <div class="columns large-3 medium-3">
            <?php get_template_part( 'parts/pieces/footer', 'socials' ); ?>
            </div><!-- /.columns large-6 -->

                    <!-- Piece-User-Nav -->
            <div class="columns large-9 medium-9">
            <?php wp_nav_menu( array( 'menu' => 'Footer User Menu'));?>
            </div><!-- /.columns large-4 -->

        </div> <!-- /.row -->

        <div class="row footer-extras expanded large-collapse medium-collapse">
            <div class="columns large-10 medium-10">
            <!-- Piece-Nav -->
                <nav role="navigation" class="show-for-medium clearfix">
                    <?php joints_footer_links(); ?>
                </nav><!-- /navigation -->
                <?php 
                // Addons
                get_template_part('parts/pieces/footer', 'addons');?>
                <?php
                echo do_shortcode('[frn_footer]'); ?>
                </div> <!--end columns -->
                
            <div class="columns large-2 medium-2">
            <?php 
            // Accreditations
            get_template_part('parts/pieces/footer', 'accreditations');?>
            </div>  <!-- end columns -->  
        </div> <!--end row -->
    </div><!-- /.inner section -->
        	
   

</footer> <!-- end .footer -->