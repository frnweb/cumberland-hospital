<?php
/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */

use \Robo\Robo;
use \Symfony\Component\Yaml\Yaml;

class RoboFile extends \Robo\Tasks
{
    private $lando_config;

    public function __construct()
    {
        Robo::loadConfiguration([__DIR__ . '/robo.yml']);
    }

    private function getLandoConfigPath() : string
    {
        return __DIR__. '/.lando.yml';
    }

    private function getLandoConfig() : array
    {
        return Yaml::parse(file_get_contents($this->getLandoConfigPath()));
    }

    private function writeLandoConfig(array $value)
    {
        $level_to_inline = 4;
        $indent_spaces = 2;
        $yml = Yaml::dump($value, $level_to_inline, $indent_spaces);
        file_put_contents($this->getLandoConfigPath(), $yml);
    }

    /**
     * Exmport database from Pantheon and import to Lando
     *
     * Default values are set in the robo.yml file.
     *
     * @option $pantheon-slug Site slug from Pantheon
     * @option $pantheon-environment Environment from which to pull database
     * @option $docker-db-container-id The Docker Container ID for the Lando Database
     * @option $db-user The Lando database username
     * @option $db-password The Lando database password
     * @option $db-name The lando database name
     */
    public function localPullDb()
    {
        // Config Values
        $lando_config     = $this->getLandoConfig();
        $terminus_command = 'vendor/bin/terminus';
        $pantheon_site    = $lando_config['config']['site'];
        $pantheon_env     = $lando_config['config']['env'];
        $pantheon_id      = "$pantheon_site.$pantheon_env";
        $db_container_id  = preg_replace('/[-_]/', '', $lando_config['name']) . "_database_1";
        $db_user          = Robo::Config()->get('lando.database.user');
        $db_password      = Robo::Config()->get('lando.database.password');
        $db_name          = Robo::Config()->get('lando.database.name');

        // Collection Items
        $collection       = $this->collectionBuilder();
        $tmp              = $collection->tmpDir();

        // Create a backup if one does not exist
        $collection->taskExec($terminus_command)
            ->printOutput(false)
            ->rawArg('backup:create')
            ->option('element', 'db')
            ->arg($pantheon_id);

        // Download the backup and save it to a temporary dir
        $collection->taskExec($terminus_command)
            ->printOutput(false)
            ->rawArg('backup:get')
            ->arg($pantheon_id)
            ->option('element', 'db')
            ->option('to', $archive = "$tmp/database.sql.gz");

        // Unzip the backup
        $collection->taskExec('gunzip')
            ->printOutput(false)
            ->rawArg($archive);

        // Fix character encoding issues
        $collection->taskExec('iconv')
            ->printOutput(false)
            ->option('from-code', 'LATIN1')
            ->option('to-code', 'UTF-8')
            ->rawArg($file = "$tmp/database.sql");

        // Import the database to the docker container with the database
        $collection->taskDockerExec($db_container_id)
            ->interactive()
            ->exec("mysql -u$db_user -p$db_password $db_name < $file");

        // EXECUTE!
        $collection->run();
    }

    /**
     * List all installed WordPress plugins
     */
    public function localListPlugins()
    {
        $this->taskExec('lando')
            ->arg('wp')
            ->arg('plugin')
            ->arg('list')
            ->run();
    }

    /**
     * Activate all installed plugins
     */
    public function localActivatePlugins()
    {
        $this->taskExec('lando')
            ->arg('wp')
            ->arg('plugin')
            ->arg('activate')
            ->option('all')
            ->run();
    }

    private function getRoboConfigPath()
    {
        return __DIR__ . '/robo.yml';
    }

    private function readRoboConfig()
    {
        return Yaml::parse(file_get_contents($this->getRoboConfigPath()));
    }

    private function writeRoboConfig(array $config)
    {
        file_put_contents($this->getRoboConfigPath(), Yaml::dump($config, 4, 2));
    }

    /**
     * Configure the Git url for the build/deploy step
     *
     * This only needs to be run once, but can be run as many
     * times as you'd like if it needs to be updated.
     */
    public function localSetGitUrl()
    {
        $robo_config = $this->readRoboConfig();
        $lando_config = $this->getLandoConfig();

        $url = $this->taskExec('terminus')
            ->rawArg('connection:info')
            ->arg($lando_config['config']['site'] . '.' . $lando_config['config']['env'])
            ->option('field', 'git_url')
            ->printOutput(false)
            ->run()
            ->getMessage();

        $robo_config['build']['git']['url'] = $url;

        $this->writeRoboConfig($robo_config);
    }

    /**
     * Returns the current tag in your local repo
     */
    public function localGetCurrentTag()
    {
        $description = $this->taskExec('git describe')
            ->printOutput(false)
            ->run()
            ->getMessage();
        $tag = preg_replace("/(.*?)-.*/", "$1", $description);
        $this->say("Current tag: $tag");
        return $tag;
    }

    /**
     * Deploy the site to Pantheon.
     *
     * Use the robo.yml file to adjust configuration as necessary
     *
     * @param string $message The commit message to include in the deploy
     */
    public function deployPantheon(string $message)
    {
        // Config Values
        $lando_config = $this->getLandoConfig();
        $pantheon_url = Robo::Config()->get("build.git.url");
        $build_dir    = Robo::Config()->get('build.dir');
        $branch       = $lando_config['config']['env'];

        // Collection Values
        $collection   = $this->collectionBuilder();
        $tmp          = $collection->workDir($build_dir);

        // STEP: Update Composer
        $collection->taskComposerUpdate()
            ->noInteraction()
            ->preferDist()
            ->optimizeAutoloader();

        // STEP: Clone Pantheon Repo
        $collection->taskGitStack()
            ->stopOnFail()
            ->cloneRepo($pantheon_url, $tmp, $branch);

        // STEP: Remove Root Wordpress Install Dirs
        $collection->taskDeleteDir(array_map(function ($item) use ($tmp) {
            return "$tmp/$item";
        }, Robo::Config()->get('build.root-wp-dirs')));

        // STEP: Remove Root Wordpress Install Files
        $collection->taskFilesystemStack()
            ->remove(array_map(function ($item) use ($tmp) {
                return "$tmp/$item";
            }, Robo::Config()->get('build.root-wp-files')));

        // STEP: Copy Essential Root Files
        array_map(function ($file) use ($collection, $tmp) {
            $collection->taskFilesystemStack()
                ->copy($file, "$tmp/$file", $force_overwrite = true);
        }, Robo::Config()->get('build.files-to-copy'));

        // STEP: Replace Existing Webroot
        $collection->taskDeleteDir("$tmp/web");
        $collection->taskCopyDir(['web' => "$tmp/web"]);

        // STEP: Replace Existing Vendor Directory
        $collection->taskDeleteDir("$tmp/vendor");
        $collection->taskCopyDir(['vendor' => "$tmp/vendor"]);

        // STEP: Replace Existing Config Directory
        $collection->taskDeleteDir("$tmp/config");
        $collection->taskCopyDir(['config' => "$tmp/config"]);

        // STEP: Delete Uploads (can't be pushed to Pantheon)
        $collection->taskDeleteDir("$tmp/web/wp-content/uploads");

        // STEP: Force Push to Pantheon
        $collection->taskGitStack()
            ->stopOnFail()
            ->dir($tmp)
            ->exec('git add -A --force')
            ->commit(Robo::Config()->get('build.git.commit-message.prefix') . ": $message -- " . $this->localGetCurrentTag())
            ->exec("git push -u origin $branch --force");

        // STEP: Clean old builds
        $collection->taskDeleteDir($build_dir);

        // EXECUTE!
        $collection->run();
    }

    /**
     * Turn on/off error reporting (default: true)
     *
     * @param string $setting Use 'true' for yes, 'false' for no.
     */
    public function localShowAllErrors(string $setting = 'true')
    {
        $path = '.env';
        $collection = $this->collectionBuilder();
        $fields = ['WP_DEBUG', 'SAVEQUERIES', 'SCRIPT_DEBUG'];

        array_map(function ($field) use ($collection, $path, $setting) {
            $collection->taskReplaceInFile($path)
                ->regex('/' . $field . '.*/')
                ->to("$field=$setting");
        }, $fields);

        $collection->run();
    }

    /**
     * List of plugins in the specified Pantheon environment
     *
     * @param string $environment The Pantheon environment name
     */
    public function remoteListPlugins(string $environment)
    {
        $slug = Robo::Config()->get('pantheon.slug');
        $this->taskExec('terminus')
            ->rawArg('remote:wp')
            ->arg("$slug.$environment")
            ->rawArg('--')
            ->arg('plugin')
            ->arg('list')
            ->run();
    }

    /**
     * Activate all plugins in the specified Pantheon environment
     *
     * @param string $environment The Pantheon environment name
     */
    public function remoteActivatePlugins(string $environment)
    {
        $slug = Robo::Config()->get('pantheon.slug');
        $this->taskExec('terminus')
            ->rawArg('remote:wp')
            ->arg("$slug.$environment")
            ->rawArg('--')
            ->arg('plugin')
            ->arg('activate')
            ->option('all')
            ->run();
    }

    /**
     * List all installed themes on a Pantheon environment
     *
     * @param string $environment The Pantheon environment name
     */
    public function remoteListThemes(string $environment)
    {
        $slug = Robo::Config()->get('pantheon.slug');
        $this->taskExec('terminus')
            ->rawArg('remote:wp')
            ->arg("$slug.$environment")
            ->rawArg('--')
            ->arg('theme')
            ->arg('list')
            ->run();
    }

    /**
     * Activate a theme on Pantheon (default: child-theme)
     *
     * @param string $environment The Pantheon environment name
     * @param string $theme The theme name (default: child-theme)
     */
    public function remoteActivateTheme(string $environment, string $theme = 'child-theme')
    {
        $slug = Robo::Config()->get('pantheon.slug');
        $this->taskExec('terminus')
            ->rawArg('remote:wp')
            ->arg("$slug.$environment")
            ->rawArg('--')
            ->arg('theme')
            ->arg('activate')
            ->arg($theme)
            ->run();
    }

    /**
     * Print whether or not repo has uncommitted changes
     */
    public function repoIsDirty() : bool
    {
        $status = $this->taskExec('git status -s')
            ->printOutput(false)
            ->run()
            ->getMessage();
        $is_dirty = ! empty($status);

        if (! $is_dirty) {
            $this->say("Repo is clean!");
            return false;
        }

        $this->say("Repo is dirty!");
        return true;
    }

    /**
     * Use Homebrew to install dependencies
     */
    public function localBrew()
    {
        $this->taskExecStack()
            ->exec('brew tap Homebrew/bundle')
            ->exec('brew bundle')
            ->run();
    }

    /**
     * Init Lando file if one doesn't exist
     */
    public function localLandoInit()
    {
        if (file_exists($this->getLandoConfigPath())) {
            $this->say('A .lando.yml file already exists!');
            return;
        }

        $this->taskExec('lando')
            ->arg('init')
            ->arg(basename(__DIR__))
            ->option('recipe', 'pantheon')
            ->option('webroot', 'web')
            ->run();
    }

    /**
     * Initialize a local Lando instance from a given Pantheon environment
     *
     * @param string $environment The name of the Pantheon environment
     */
    public function localSetup(string $environment = 'slate')
    {
        // Init variables
        $collection = $this->collectionBuilder();

        $collection->addCode(function () {
            $this->localBrew();
        });

        $collection->addCode(function () {
            $this->localLandoInit();
        });

        // Setup Lando Config after we know it exists
        $collection->addCode(function () {
            $this->lando_config = $this->getLandoConfig();
        });

        // Setup new Git Repo
        $collection->taskExec('git init');
        if ($this->repoIsDirty()) {
            $collection->taskGitStack()
                ->add('-A')
                ->commit('Initial commit');
        }
        $collection->taskExec('git flow init -d');

        // Ensure correct environment is set
        $collection->addCode(function () use ($environment) {
            $env = &$this->lando_config['config']['env'];
            $env = ($env == $environment) ? $env : $environment;
            $this->writeLandoConfig($this->lando_config);
        });

        // Setup default environment variables
        $collection->taskFilesystemStack()
            ->copy('.env.example', '.env');

        // Set WP_HOME from lando config
        $collection->addCode(function () {
            $this->localSetWpHome();
        });

        // Composer install
        $collection->taskComposerInstall();

        // Create a new multidev (if it doesn't exist)
        $collection->addCode(function () use ($environment) {
            $this->lando_config = $this->getLandoConfig();
            $this->taskExec('terminus')
                ->rawArg('multidev:create')
                ->arg($this->lando_config['config']['site'] . '.dev')
                ->rawArg($environment)
                ->run();
        });

        // Rebuild Lando
        $collection->taskExec('lando')
            ->arg('rebuild')
            ->rawArg('-y');

        // Set Git URL in config for deploy
        $collection->addCode(function () {
            $this->localSetGitUrl();
        });

        // Pull the DB from the environment
        $collection->addCode(function () {
            $this->localPullDb();
        });

        // Activate all installed plugins
        $collection->addCode(function () {
            $this->localActivatePlugins();
        });


        // Activate the child theme
        $collection->addCode(function () {
            $this->localActivateTheme();
        });

        // EXECUTE!
        $collection->run();
    }

    /**
     * Set WP_HOME in .env from Lando config
     */
    public function localSetWpHome()
    {
        $this->lando_config = $this->getLandoConfig();

        $this->taskReplaceInFile('.env')
            ->regex('/^WP_HOME.*?$/m')
            ->to('WP_HOME=http://' . $this->lando_config['name'] . '.lndo.site')
            ->run();
    }

    /**
     * List all themes installed in the Lando instance
     */
    public function localListThemes()
    {
        $this->taskExec('lando')
            ->arg('wp')
            ->arg('theme')
            ->arg('list')
            ->run();
    }

    /**
     * Activate a given theme (default: child-theme)
     *
     * @param string $theme_name The slug of the theme to activate
     */
    public function localActivateTheme(string $theme_name = 'child-theme')
    {
        $this->taskExec('lando')
            ->arg('wp')
            ->arg('theme')
            ->arg('activate')
            ->rawArg("'$theme_name'")
            ->run();
    }

    /**
     * Pull media content from Pantheon into local wp-content/uploads
     */
    public function localPullFiles()
    {
        // Collection Vars
        $collection = $this->collectionBuilder();
        $tmp        = $collection->tmpDir();
        $filename   = 'files';

        // Config Vars
        $lando_config            = $this->getLandoConfig();
        $site                    = $lando_config['config']['site'];
        $env                     = $lando_config['config']['env'];
        $site_id                 = "$site.$env";
        $archive_sub_folder_name = "{$filename}_{$env}";
        $uploads_dir             = __DIR__ . '/web/wp-content/uploads/';

        // Create a backup on Pantheon, download, and extract it.
        $collection->taskExecStack()
            ->exec("terminus backup:create $site_id")
            ->exec("terminus backup:get $site_id --element files --to $tmp/$filename.tar.gz")
            ->exec("tar xzf $tmp/$filename.tar.gz -C $tmp");

        // Overwrite the existing uploads directory
        $collection->taskCopyDir(["$tmp/$archive_sub_folder_name" => $uploads_dir])->overwrite(true);

        // EXECUTE!
        $collection->run();
    }
}
