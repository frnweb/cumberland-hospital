# Slate Sandbox

A boilerplate for Slate-based projects

---

## Installation

### Software Tools

* Required
    * [Composer](https://getcomposer.org/)
    * [Lando](https://docs.lndo.io/installation/installing.html)
    * [Homebrew](https://brew.sh/)
* Recommended (install with Homebrew)
    * Z-shell: `brew install zsh`
    * iTerm2: `brew cask install iterm2`
    * [Prezto for Zshell](https://github.com/sorin-ionescu/prezto)

### Pre-requisites

**TL;DR**: Configure Composer to point at our private package registry.

Because some of the code on which we depend is a proprietary to FRN or require paid licenses, we use Private Packagist in order to host all of our dependent code. Composer must be configured to look here first. To do so, complete the following steps:

* Authorize Composer with the following command: `composer config --global --auth http-basic.repo.packagist.com frnutz 32db4993112e175056bc6fafea797603d549662563cfb757aa8328cbc6cd`
* Open your global Composer configuration with the following command: `open ~/.composer/config.json`.
* Copy and paste the follwing into the `config.json` file, overwriting whatever was in there:

```json
{
    "repositories": [
        {"type": "composer", "url": "https://repo.packagist.com/foundations-recovery/"},
        {"packagist.org": false}
    ],
    "config": {}
}
```

*Note: You should only have to do this once per computer. If you update the OS, get a new laptop, etc, you might need to re-do this step!*

### Basic Installation

Execute the following commands in your shell, where `project-name` is the name of your project. **Note:** *Each line is it's own command.*

```bash
cd ~/Sites
composer create-project frnweb/slate-sandbox project-name
cd project-name
./vendor/bin/robo local:setup
## OR, if you have robo installed on your machine already ##
robo local:setup
```

Be aware that the last command requires some additional interaction, and can take awhile to complete. Be patient!

**Note:** *this will install the project in your `~/Sites` directory. If you'd like it installed elsewhere, simply change the first command to any directory you'd prefer.*

### Advanced Installation Notes

* In order to share this site, create a new [BitBucket](http://bitbucket.org/frnweb) repository, copy the URL for your new repository, and execute the following command: `git remote add origin URL` where "URL" is the BitBucket git url. (ex. `http://bitbucket.org/frnweb/peachford.git`)
* If you're having trouble during the `local:setup`, go into the `~/Sites/project-name` directory and run `brew bundle`. This will install all of the OSX utilities necessary to run the builds.

### Database Import Issues

The local setup should pull the database from Pantheon and install it to your local dev instance. If WordPress still appears to be uninstalled, there may be a mis-match in the table-prefix that was used when the original site was installed. (Our default is `wp_`, like WordPress).

To fix this follow these steps:

#### 1. Inspect the installed database

Enter the commands below. The first command will open a MySQL shell, so the commands will look a bit different than your normal shell commands. Nevertheless, enter them in order and hit the enter key each time.

```bash
lando mysql
USE pantheon;
SHOW tables;
```

#### 2. Find the table prefix

At this point, you should see a list of tables. Most of those tables will start with a prefix that is the same. Note that prefix if it is different than `wp_`, type `exit`, and then hit enter to leave MySQL.

#### 3. Edit your Environment Variable

Open the `.env` file in your favorite text editor. Find the line that says `DB_PREFIX=wp_`. Edit the value after the `=` to match the prefix you noted earlier. Save that file and refresh your browser. Hopefully your database should be intact!

---

## Usage

### Error Reporting

**TL;DR**: To silence errors, type `robo local:show-all-errors false` in your terminal.

We have a few great tools to help debug errors that are thrown in the development process. Unfortunately, some of the plugins upon which we depend throw complier errors, warnings, and notices. If they are installed and active, they will make it look like our site is broken.

In order to turn off those errors, you have two choices:

* Run `robo local:show-all-errors false` in your terminal.
* Edit the `.env` file in the project root to your preferences.

### Adding/Removing Plugins

**TL;DR**: To install a plugin, type `composer require wpackagist-plugin/PLUGINSLUG`.

You can install any plugin listed on the WordPress Plugin Directory using the above code, where `PLUGINSLUG` is the name of the plugin slug. If you're unsure what the slug is, you can search on [WPackagist](https://wpackagist.org) to find what you're looking for.

After a plugin is installed, a script will run to automatically activate it through [WordPress CLI](http://wp-cli.org/). There are a lot of great commands to explore there! To run a WP-CLI command on your local Lando site, simply type `lando wp` and the command. (ex. `lando wp plugin activate --all`).

### Robo

**TL;DR**: In the `~/Sites/project-name` directory, type `robo` to see a list of available commands.

[Robo](http://robo.li) is a task runner like Grunt or Gulp, but for PHP projects. We use this task runner to automate simple, repetitive tasks throughout the course of development.

### Theme Development

**TL;DR**: Add your site-specific files in the `~/Sites/project-name/web/wp-content/themes/child-theme/` directory.

### Git Flow

The site repo is setup to follow the [GitFlow](https://datasift.github.io/gitflow/IntroducingGitFlow.html) workflow for collaborative development. There are both command-line and GUI tools for git that facilitate this process.
