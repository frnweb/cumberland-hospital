<?php

/**
 * @var string Directory containing all of the site's files
 */
$root_dir = dirname(__DIR__);

/**
 * @var string Root directory actually served to the web
 */
$webroot_dir = "$root_dir/web";

/**
 * Expose global env() function from oscarotero/env
 */
Env::init();

/**
 * Use Dotenv to set required environment variables and load .env file in root
 */
$dotenv = new Dotenv\Dotenv($root_dir);
if (file_exists($root_dir . '/.env')) {
    $dotenv->overload();
    $dotenv->required([
        'WP_ENV',
        'DB_NAME',
        'DB_USER',
        'DB_PASSWORD',
        'DB_HOST',
        'DB_PORT',
        'DB_PREFIX',
        'WP_HOME',
        'WP_SITEURL'
    ]);
}

/**
 * Set up our global environment constant and load its config first
 * Default: pantheon
 */
define('WP_ENV', env('WP_ENV') ?? 'pantheon');
$env_config = __DIR__ . '/environments/' . WP_ENV . '.php';
if (file_exists($env_config)) {
    require_once $env_config;
}

/**
 * MySQL Settings
 */
define('DB_NAME', env('DB_NAME'));
define('DB_USER', env('DB_USER'));
define('DB_PASSWORD', env('DB_PASSWORD'));
define('DB_HOST', env('DB_HOST')
    . ':'
    . env('DB_PORT'));
define('DB_CHARSET', env('DB_CHARSET')
    ?? 'utf8mb4');
define('DB_COLLATE', '');

/**
 * Authentication Unique Keys and Salts
 */
define('AUTH_KEY', env('AUTH_KEY'));
define('SECURE_AUTH_KEY', env('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY', env('LOGGED_IN_KEY'));
define('NONCE_KEY', env('NONCE_KEY'));
define('AUTH_SALT', env('AUTH_SALT'));
define('SECURE_AUTH_SALT', env('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT', env('LOGGED_IN_SALT'));
define('NONCE_SALT', env('NONCE_SALT'));

/**
 * Custom Content Directories
 */
define('CONTENT_DIR', '/wp-content');
define('WP_CONTENT_DIR', $webroot_dir . CONTENT_DIR);
define('WP_CONTENT_URL', WP_HOME . CONTENT_DIR);

/**
 * Custom Settings
 */
define('AUTOMATIC_UPDATER_DISABLED', true);
define('WP_AUTO_UDPATE_CORE', false);
define('DISABLE_WP_CRON', env('DISABLE_WP_CRON') ?? false);
define('DISALLOW_FILE_EDIT', true);

/**
 * WordPress Database Table prefix.
 */
$table_prefix = env('DB_PREFIX')
    ?? 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');
