<?php

/**
 * URLs
 */
define('WP_HOME', env('WP_HOME'));
define('WP_SITEURL', env('WP_SITEURL'));

/**
 * Debug Mode
 */
define('WP_DEBUG', env('WP_DEBUG') ?? true);
define('SAVEQUERIES', env('SAVEQUERIES') ?? true);
define('SCRIPT_DEBUG', env('SCRIPT_DEBUG') ?? true);

/**
 * Pretty Error Reporting
 */
if (env('PRETTY_ERRORS') ?? true) {
    $whoops = new \Whoops\Run;
    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
    $whoops->register();
}
