<?php

/**
 * URLs
 */
if (isset($_SERVER['HTTP_HOST'])) {
    // HTTP is still the default scheme for now.
    $scheme = 'http';
    // If we have detected that the end use is HTTPS, make sure we pass that
    // through here, so <img> tags and the like don't generate mixed-mode
    // content warnings.
    if (isset($_SERVER['HTTP_USER_AGENT_HTTPS']) && $_SERVER['HTTP_USER_AGENT_HTTPS'] == 'ON') {
        $scheme = 'https';
    }
    define('WP_HOME', $scheme
        . '://'
        . $_SERVER['HTTP_HOST']);
    define('WP_SITEURL', $scheme
        . '://'
        . $_SERVER['HTTP_HOST']
        . '/wp');
}

/**
 * Force the use of a safe temp directory when in a container
 */
if (defined('PANTHEON_BINDING')) {
    define('WP_TEMP_DIR', sprintf('/srv/bindings/%s/tmp', PANTHEON_BINDING));
}

/**
 * FS writes aren't permitted in test or live, so we should let WordPress know to disable relevant UI
 */
if (in_array(env('PANTHEON_ENVIRONMENT'), ['test', 'live'])
    && ! defined('DISALLOW_FILE_MODS')
) {
    define('DISALLOW_FILE_MODS', true);
}

/**
 * NO Debug Mode in Test or Live
 */
if (in_array(env('PANTHEON_ENVIRONMENT'), ['test', 'live'])) {
    define('WP_DEBUG', true);
    define('WP_DEBUG_DISPLAY', false);
    define('WP_SCRIPT_DEBUG', false);
} else {
    define('WP_DEBUG', false);
    define('WP_DEBUG_DISPLAY', true);
    define('WP_SCRIPT_DEBUG', true);
}
